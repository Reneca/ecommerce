﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Ecommerce.Models
{
    public class City
    {
        [Key]
        public int CityId { get; set; }
        [Required(ErrorMessage = " the field {0} is required")]
        [MaxLength(50, ErrorMessage = " the field{0} must be at maxium {1} characters length")]
        [Display(Name = "City")]
        public String Name { get; set; }
        [Required(ErrorMessage = " the field {0} is required")]
        public int DepartamentId { get; set; }
        public virtual Department Deparment { get; set; }

    }
}