﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Ecommerce.Models
{
    public class Department
    {
        [Key]
        public int DepartamentId { get; set; }
        [Required(ErrorMessage =" the field {0} is required")]
        [MaxLength(50,ErrorMessage =" the field{0} must be at maxium {1} characters length")]
        [Display(Name="Department")]
        public String Name { get; set; }
        public virtual ICollection<City> Cities { get; set; }
    }
}